from fastapi import FastAPI
from pydantic import BaseModel
from textblob import TextBlob

app = FastAPI()


class Review(BaseModel):
    text: str
    stars: int


@app.post("/calculate_review_score")
async def analyze_sentiment(review: Review):
    text = review.text
    stars = int(review.stars)

    if stars > 5 or stars < 0:
        return {"error": "Stars must be between 0 and 5"}

    stars_score = calculate_stars_score(stars)

    blob = TextBlob(text)
    review_sentiment_multiplier = blob.sentiment.polarity
    sentiment_score = review_sentiment_multiplier * 5 + stars_score
    return {"review_score": sentiment_score}


def calculate_stars_score(stars):
    return stars * 2 - 5
